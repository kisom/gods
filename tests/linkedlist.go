package main

import (
	"fmt"
	"gods/linkedlist"
	"os"
	"strconv"
)

func main() {
	/*
	   head := new(linkedlist.Node)
	   head = linkedlist.Append(head, 5)
	   head = linkedlist.Append(head, 25)
	   head = linkedlist.Append(head, "hello")
	*/

	/*
		head := new(linkedlist.Node)
		linkedlist.Append(head, 5)
		linkedlist.Append(head, 25)
		linkedlist.Append(head, "hello")

		fmt.Println("[+] linked list test")
		for cursor := head; cursor.Next != nil; {
			fmt.Println(cursor)
			fmt.Println(cursor.Data)
			cursor = cursor.Next
		}
	*/

	fmt.Println("[+] linked list test")
	list := linkedlist.New()
	list.Append(0)
	fmt.Println("[ LIST ]")
	list.Print()
	list.Insert(5)
	fmt.Println("[ LIST ]")
	list.Print()
	list.Insert(10)
	fmt.Println("[ LIST ]")
	list.Print()
	list.Append(15)
	fmt.Println("[ LIST ]")
	list.Print()

	if len(os.Args) > 1 {
		key, _ := strconv.Atoi(os.Args[1])
		if node := list.Search(key); node != nil {
			fmt.Printf("[+] found %d\n", key)
			node.InsertAfter("MARKER")
		}
	}
	fmt.Println(":[ LIST ]:")
	list.Print()

	fmt.Println("[+] removing all")
	list.Remove(10)
	fmt.Println("[ LIST ]")
	list.Print()
	list.Remove(10)
	list.Remove(5)
	list.Remove(15)
	list.Remove("MARKER")
	list.Remove(0)

	fmt.Println("[ LIST ]")
	list.Print()

}
