package main

import (
	"fmt"
	"gods/linkedlist2"
	"os"
	"strconv"
)

func main() {
	dump_after := true
	fmt.Println("[+] doubly-linked list test")
	fmt.Println("[+] creating list")
	list := linkedlist2.New(nil)

	fmt.Println("[+] appending 0")
	list.Append(0)
	if dump_after {
		fmt.Println("[ LIST ]")
		list.Print()
		fmt.Printf("\t%d elements\n", list.Len())
	}

	fmt.Println("[+] inserting 5")
	list.Insert(5)
	if dump_after {
		fmt.Println("[ LIST ]")
		list.Print()
	}

	fmt.Println("[+] inserting 10")
	list.Insert(10)
	if dump_after {
		fmt.Println("[ LIST ]")
		list.Print()
	}

	fmt.Println("[+] appending 15")
	list.Append(15)

	fmt.Println("[+] appending 20")
	list.Append(20)

	if dump_after {
		fmt.Println("[ LIST ]")
		list.Print()
	}

	if len(os.Args) > 1 {
		key, _ := strconv.Atoi(os.Args[1])
		if node := list.Search(key); node != nil {
			fmt.Printf("[+] found %d\n", key)
			node.InsertAfter("MARKER")
		}
	}
	fmt.Println("[+] removing 10")
	list.Remove(10)
	if dump_after {
		fmt.Println("[ LIST ]")
		list.Print()
	}

	fmt.Println("[+] removing 20")
	list.Remove(20)
	if dump_after {
		fmt.Println("[ LIST ]")
		list.Print()
	}

	fmt.Println("[+] removing 0")
	list.Remove(0)
	if dump_after {
		fmt.Println("[ LIST ]")
		list.Print()
	}

	node := list.Search(5)
	list.RemoveNode(node)
	if dump_after {
		fmt.Println("[ LIST ]")
		list.Print()
	}

	node = list.Search(15)
	node.InsertAfter(3)
	if dump_after {
		fmt.Println("[ LIST ]")
		list.Print()
	}

	fmt.Println("[+] removing rest of list")
	list.Remove(15)
	list.Remove(3)

	if list.Len() == 0 {
		fmt.Println("[+] list emptied successfully!")
	} else {
		fmt.Println("[!] list not empty!")
		list.Print()
	}

	list.Remove(nil)
}
