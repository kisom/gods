package main

import (
	"fmt"
	"gods/stack"
)

func main() {
	s := new(stack.Stack)

	fmt.Println("[+] validating error returned on peeking empty stack")
	if _, err := s.Peek(); err == nil {
		fmt.Println("[!] expected error, but no error was given.")
	} else {
		fmt.Println("[+] expected error thrown: ", err)
	}
	fmt.Println("[+] pushing 0")
	s.Push(0)
	s.Print()

	fmt.Println("[+] pushing 5")
	s.Push(5)
	s.Print()

	fmt.Println("[+] peeking")
	if peekv, err := s.Peek(); err != nil {
		fmt.Println("[!] unexpected error: ", err)
	} else if peekv != 5 {
		fmt.Println("[!] unexpected top value: ", peekv)
	} else {
		fmt.Println("[+] saw expected (5) as top value.")
	}
	fmt.Println("[+] popping")
	top, err := s.Pop()
	if err == nil {
		fmt.Printf("\t[*] top element: ")
		fmt.Println(top)
	} else {
		fmt.Println("[!] error popping from stack: ", err)
	}
	fmt.Println("stack")
	s.Print()
}
