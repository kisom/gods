package main

import (
	"fmt"
	"gods/graph"
)

func main() {
	g := graph.UndirectedGraph()
	fmt.Println("[+] adding next ID: ", g.NextID())
	g.AddNode(g.NextID())
	fmt.Println("[+] adding next ID: ", g.NextID())
	next := g.NextID()
	g.AddNode(next)
	g.Name(next, "Beta")
	fmt.Println("[+] adding next ID: ", g.NextID())
	if err := g.AddNamed(g.NextID(), "Gamma"); err != nil {
		fmt.Println("[!] error adding named: ", err)
	}

	g.Link(0, 2)
	g.Link(1, 2)

	fmt.Print("[+] 0 is connected to 1: ")
	if connected, _ := g.Linked(0, 1); connected {
		fmt.Println("yes")
		fmt.Println("[!] relationship should not exist!")
	} else {
		fmt.Println("no")
	}

	fmt.Print("[+] 0 is connected to 2: ")
	if connected, err := g.Linked(0, 2); err != nil {
		fmt.Println(err)
	} else if !connected {
		fmt.Println("no")
		fmt.Println("[!] relationship should exist!")
	} else {
		fmt.Println("yes")
	}
	g.Dump()
}
