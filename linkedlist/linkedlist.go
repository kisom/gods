// linkedlist provides a singly-linked list.
//
// New lists should be created with the New() function.
package linkedlist

import (
	"fmt"
)

// Implementation of a singly-linked list.
type LinkedList struct {
	Head *Node
}

// Node represents a single node in the list.
type Node struct {
	Data interface{}
	Next *Node
}

// New creates a properly ninitialised LinkedList
func New() *LinkedList {
	list := new(LinkedList)
	list.Head = new(Node)
	return list
}

// Print will display a node as its data.
func (node *Node) Print(desc string) {
	fmt.Print(desc)
	if node != nil {
		fmt.Println(node.Data)
	} else {
		fmt.Println("<NULL>")
	}
}

// Print walks through the list and calls prints each node and its position
// in the list.
func (list *LinkedList) Print() {
	cursor := list.Head
	chain := 0
	for ; cursor.Next != nil; cursor = cursor.Next {
		cursor.Print(fmt.Sprintf("%d : ", chain))
		chain++
	}
}

// Insert creates a new node at the head of the list.
func (list *LinkedList) Insert(data interface{}) {
	cursor := new(Node)
	cursor.Data, cursor.Next = data, list.Head
	list.Head = cursor
}

// InsertAfter will insert a node after the node it is called on.
func (node *Node) InsertAfter(data interface{}) {
	next := node.Next
	new_node := new(Node)
	new_node.Data, new_node.Next = data, next
	node.Next = new_node
}

// Append adds a new node at the end of the list.
func (list *LinkedList) Append(data interface{}) {
	cursor := list.Head
	tmp := cursor

	if cursor.Data == nil && cursor.Next == nil {
		node := new(Node)
		node.Data, node.Next = data, list.Head
		list.Head = node
		return
	}

	if cursor.Next != nil {
		for ; cursor.Next != nil; cursor = cursor.Next {
			tmp = cursor
		}
	}

	node := new(Node)
	node.Data, node.Next = data, cursor
	tmp.Next = node
}

// Search traverses the list, checking whether a node with the given data
// is present.
func (list *LinkedList) Search(target interface{}) *Node {
	cursor := list.Head
	for ; cursor.Next != nil; cursor = cursor.Next {
		if cursor.Data == target {
			return cursor
		}
	}

	return nil
}

// Remove traverses the list and removes the first node that contains
// the specified target value.
func (list *LinkedList) Remove(target interface{}) {
	// head of list, end of list, middle of list
	cursor := list.Head

	if cursor == nil {
		return
	} else if cursor.Data == target {
		list.Head = cursor.Next
	}

	for ; cursor.Next != nil; cursor = cursor.Next {
		if cursor.Next.Data == target {
			cursor.Next = cursor.Next.Next
		}
	}
}
