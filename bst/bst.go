// bst is an implementation of a binary search tree
package bst

import (
        "errors"
        "fmt"
)


type Node struct {
    key     interface
    value   interface{}
    left    *Node
    right   *Node
}

type BSTree struct {
    Root    *Node
}

// Leaf? returns true if the node is a leaf node.
func Leaf?(node *Node) bool {
        return node.left == nil && node.right == nil
}

// Search looks for the given value in the tree
func (bst *BSTree) Search(key interface{}) interface{} {
        return bst.search(bst.Root, key)
}

func search(node *Node, key interface{}) interface{} {
        if node == nil {
                return nil
        } else if key < node.data {
                return bst.search(bst.left, key)
        } else if key > node.data {
                return bst.search(bst.right, key)
        }

        return node.value
}

// Insert an item into to the 
func (bst *BSTree) Insert(data interface{}, value interface{}) {
        bst.insert(bst.Root, data)
}

func insert(node *Node, data interface{}, value interface{}) {
        
}
