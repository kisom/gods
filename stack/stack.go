// Stack implementation in Go.
package stack

import (
	"errors"
	"fmt"
)

// Stack is the foundation data type for the stack.
type Stack struct {
	stack []interface{}
}

// Print will print the items on the list as well as the list length and
// capacity.
func (stack *Stack) Print() {
	for i := len(stack.stack) - 1; i >= 0; i-- {
		fmt.Printf("\t")
		fmt.Println(stack.stack[i])
	}
	fmt.Printf("\tlength / capacity: %d / %d\n", stack.Len(), stack.Cap())
}

// Len returns the length of the stack.
func (stack *Stack) Len() int {
	return len(stack.stack)
}

// Cap returns the capacity of the stack.
func (stack *Stack) Cap() int {
	return cap(stack.stack)
}

// Pop removes the top item off the stack and returns it.
func (stack *Stack) Pop() (interface{}, error) {
	length := len(stack.stack)
	if length > 0 {
		element := stack.stack[length-1]
		stack.stack = stack.stack[:length-1]
		return element, nil
	}

	return nil, errors.New("stack is empty")
}

// Push adds a new item on the stack.
func (stack *Stack) Push(data interface{}) {
	stack.stack = append(stack.stack, data)
}

// Peek returns the value of the top item on the stack.
func (stack *Stack) Peek() (interface{}, error) {
	if stack.Len() == 0 {
		return nil, errors.New("stack is empty")
	}

	return stack.stack[len(stack.stack)-1], nil
}
