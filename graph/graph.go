/*
   Package graph implements a simple graph data structure. It uses a int64s
   as node identifiers and provides a simple naming interface.
*/
package graph

import (
	"errors"
	"fmt"
	"gods/linkedlist2"
)

// Graph is the base data structure. It has a number of methods for interaction
// and manipulation.
type Graph struct {
	// TODO: add name mapping
	nodes       *linkedlist2.LinkedList
	connections *linkedlist2.LinkedList
	names       map[int64]string
	directed    bool
}

// UndirectedGraph initialises and returns a Graph reference setup as an 
// undirected graph.
func UndirectedGraph() *Graph {
	graph := new(Graph)
	graph.nodes = linkedlist2.New(nil)
	graph.connections = linkedlist2.New(nil)
	graph.directed = false
	graph.names = make(map[int64]string)
	return graph
}

// DirectedGraph initialises and returns a Graph reference setup as a directed
// graph.
func DirectedGraph() *Graph {
	graph := new(Graph)
	graph.nodes = linkedlist2.New(nil)
	graph.connections = linkedlist2.New(nil)
	graph.directed = true
	graph.names = make(map[int64]string)
	return graph
}

// Edge represents a connection between two nodes.
type Edge struct {
	from int64
	to   int64
}

// Get returns the two nodes in an edge as a slice in the order {from, to}.
func (edge Edge) Get() []int64 {
	return []int64{edge.from, edge.to}
}

// Link sets up an edge between two nodes and stores the edge in the graph.
func (graph *Graph) Link(from int64, to int64) error {
	if err := graph.Known(from); err != nil {
		return err
	} else if err = graph.Known(to); err != nil {
		return err
	}

	if err := graph.link_to(from, to); err != nil {
		return err
	}

	if !graph.directed {
		if err := graph.link_to(to, from); err != nil {
			return err
		}
	}

	return nil
}

// link_to is a graph primitive for setting up a one-way connection.
func (graph *Graph) link_to(from int64, to int64) error {
	if err := graph.Known(from); err != nil {
		return err
	} else if err = graph.Known(to); err != nil {
		return err
	}

	edge := Edge{from, to}

	if graph.connections.Search(edge) != nil {
		return errors.New(fmt.Sprintf("edge %d->%d already exists", from, to))
	}

	graph.connections.Insert(edge)
	return nil
}

// Linked returns true if the two nodes specified are linked by an edge.
func (graph *Graph) Linked(from int64, to int64) (bool, error) {
	n, err := graph.LinkCount(from, to)
	if err != nil {
		return false, err
	}
	return n > 0, nil
}

// LinkCount returns the number of direct edges between the two nodes. In an
// undirected graph, the maximum number of connections is 1; in a directed
// graph, the maximum number of connections is 2. 
//
// Given n <- number of connections:
//      0: nodes are not connected
//      1: in an unidrected graph, the nodes are connected. in a directed
//         graph, a one-way connection exists between the nodes.
//      2: in a directed graph, a two-way (mutual) connection exists between
//         the nodes.
func (graph *Graph) LinkCount(from int64, to int64) (int, error) {
	if err := graph.Known(from); err != nil {
		return 0, err
	} else if err = graph.Known(to); err != nil {
		return 0, err
	}

	edge := Edge{from, to}
	connections := 0

	if graph.connections.Search(edge) != nil {
		connections++
	}

	if graph.directed {
		edge = Edge{to, from}
		if graph.connections.Search(edge) != nil {
			connections++
		}
	}

	return connections, nil
}

// unlink_from removes the connection between two nodes; this only removes
// the one-way connection and should be called from Unlink to ensure in the
// case of an undirected graph, both connections are removed.
func (graph *Graph) unlink_from(from int64, to int64) error {
	if err := graph.Known(from); err != nil {
		return err
	} else if err = graph.Known(to); err != nil {
		return err
	}

	edge := Edge{from, to}
	if node := graph.connections.Search(edge); node != nil {
		return errors.New(fmt.Sprintf("edge %d->%d doesn't exist", from, to))
	} else {
		graph.connections.RemoveNode(node)
	}
	return nil
}

// Unlink removes the connection between two nodes. In an undirected graph,
// this will remove both connections. In a directed graph, this removes only
// the one-way connection.
func (graph *Graph) Unlink(from int64, to int64) error {
	if err := graph.Known(from); err != nil {
		return err
	} else if err = graph.Known(to); err != nil {
		return err
	}

	if err := graph.unlink_from(from, to); err != nil {
		return err
	}

	if !graph.directed {
		if err := graph.unlink_from(to, from); err != nil {
			return err
		}
	}

	return nil
}

// Known determines whether a node is exists in the graph (i.e. whether the
// graph "knows about" the node). It returns nil if the node is known, and
// returns an error otherwise. It is used internally in many functions to
// determine whether it is safe to check for connections.
func (graph *Graph) Known(id int64) error {
	if node := graph.nodes.Search(id); node == nil {
		return errors.New(fmt.Sprintf("unknown node %d", id))
	}

	return nil
}

// AddNode adds a node to the graph.
func (graph *Graph) AddNode(id int64) error {
	if graph.nodes.Search(id) != nil {
		return errors.New(fmt.Sprintf("node %d already exists", id))
	}

	graph.nodes.Insert(id)
	return nil
}

// Dump is a debugging utility to dump the current state of the graph.
func (graph *Graph) Dump() {
	fmt.Println("[ NODES ]")
	for cur := graph.nodes.Tail(); cur != nil; cur = cur.Prev {
		node := cur.Data.(int64)
		name, _ := graph.NameOf(node)
		fmt.Printf("\t%d: %s\n", node, name)
	}
	fmt.Println("[ CONNECTIONS ]")
    old_cur := graph.connections.Cur()
    conn := graph.connections
    for conn.Head(); conn.Cur() != nil; conn.Next() {
    		edge := conn.Get().(Edge)
    		fmt.Printf("\t%d->%d\n", edge.from, edge.to)
    }
    conn.SetCur(old_cur)
}

// NextID returns the next highest :node ID in the graph, i.e. for obtaining
// a suitable ID for a new node.
func (graph *Graph) NextID() int64 {
	id_list := []int64{}

	nodes := graph.nodes

	for nodes.Head(); nodes.Get() != nil; nodes.Next() {
		id_list = append(id_list, nodes.Get().(int64))
	}

	if len(id_list) == 0 {
		return 0
	}

	max := int64(0)
	for i := 0; i < len(id_list); i++ {
		if id_list[i] > max {
			max = id_list[i]
		}
	}
	return max + 1
}

// Name will name the given node.
func (graph *Graph) Name(id int64, name string) error {
	if err := graph.Known(id); err != nil {
		return err
	}

	graph.names[id] = name
	return nil
}

// NameOf will look up the current node's name.
func (graph *Graph) NameOf(id int64) (string, error) {
	if err := graph.Known(id); err != nil {
		return "", err
	}

	name, present := graph.names[id]
	if present == false {
		return "<anonymous>", nil
	}
	return name, nil
}

// AddNamed will add a named node to the graph
func (graph *Graph) AddNamed(id int64, name string) error {
	if err := graph.AddNode(id); err != nil {
		return err
	} else if err = graph.Name(id, name); err != nil {
		return err
	}

	return nil
}
