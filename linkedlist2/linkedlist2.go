// doubly linked list implementation in Go
//
// New lists should be created with the New() function to ensure they are 
// properly initialised.
package linkedlist2

import (
	"fmt"
)

// LinkedList provides a doubly-linked linked-list. 
type LinkedList struct {
	head   *Node
	cursor *Node
}

// Node represents a single node in the linked list.
type Node struct {
	Data interface{}
	Prev *Node
	Next *Node
}

// Head resets the list cursor to the head of the list.
func (list *LinkedList) Head() {
        list.cursor = list.head
}

// Next moves one link in the list and returns the value of the cursor at its
// new position.
func (list *LinkedList) Next() interface{} {
        if !list.cursor.Null() {
                list.cursor = list.cursor.Next
        }

        return list.cursor.Data
}

func (list *LinkedList) Prev() interface{} {
        if list.cursor.Prev != nil {
                list.cursor = list.cursor.Prev
        }

        return list.cursor.Data
}

// Get returns the current value of the list.
func (list *LinkedList) Get() interface{} {
        return list.cursor.Data
}

// Cur returns the current pointer (i.e. for creating a snapshot of the
// current location in the list
func (list *LinkedList) Cur() *Node {
		if list.cursor == nil || list.cursor.Null() {
				return nil
		}
        return list.cursor
}

func (list *LinkedList) SetCur(node *Node) error {
		old_cur := list.cursor
		valid := false
		for list.Head(); list.Get() != nil; list.Next() {
				if list.cursor == node {
					valid = true
					list.cursor = node
				}
		}

		if !valid {
				list.cursor = old_cur
				return fmt.Errorf("node not in list")
		}

		return nil

}

// New creates a new LinkedList, initialising the head.
func New(node *Node) *LinkedList {
	list := new(LinkedList)
	if node == nil {
		list.head = new(Node)
	} else {
		// this isn't right: the node represents a new linked list;
		// should it be copied, duplicating this node and setting
		// node.Prev -> nil, or be left as is?
		list.head = node
	}

	return list
}

// Print applied to a node pretty-prints the node.
func (node *Node) Print(desc string) {
	fmt.Print(desc)
	if node != nil {
		fmt.Print("data: ", node.Data, " | prev: ", &(node.Prev))
		fmt.Println(" | next: ", &(node.Next))
	} else {
		fmt.Println("<NULL>")
	}
}

// Dump called on a Node is a debugging aid that prints the raw nod
// information.
func (node *Node) Dump(desc string) {
	fmt.Print(desc)
	if node != nil {
		fmt.Print("data: ", node.Data, " | prev: ", node.Prev)
		fmt.Println(" | next: ", node.Next)
	} else {
		fmt.Println("<NULL>")
	}
}

// Null determines whether a node is the end-of-list terminator.
func (node *Node) Null() bool {
	if node.Data == nil && node.Next == nil {
		return true
	} else {
		return false
	}
	return false // should not get here
}

// Dump on a LinkedList walks through the list, calling Dump for each node.
func (list *LinkedList) Dump() {
	cursor := list.head
	chain := 0
	for ; cursor.Next != nil; cursor = cursor.Next {
		fmt.Println(fmt.Sprintf("%d : ", chain), cursor)
		chain++
	}
}

// Print on a LinkedList walks through the list, calling Print for each
// node.
func (list *LinkedList) Print() {
	cursor := list.head
	chain := 0
	for ; cursor.Next != nil; cursor = cursor.Next {
		cursor.Print(fmt.Sprintf("\t%d : ", chain))
		chain++
	}
}

// Len provides the length of the LinkedList
func (list *LinkedList) Len() int {
	count := 0

	for cursor := list.head; cursor.Next != nil; cursor = cursor.Next {
		count++
	}

	return count
}

// Search the LinkedList for the given target.
func (list *LinkedList) Search(target interface{}) *Node {
	for cursor := list.head; cursor.Next != nil; cursor = cursor.Next {
		if cursor.Data == target {
			return cursor
		}
	}

	return nil
}

// Insert will insert a new nnode at the beginning of the list with
// compleixity 1.
func (list *LinkedList) Insert(data interface{}) {
	head := list.head
	node := new(Node)
	node.Prev = nil
	node.Next = head
	node.Data = data
	head.Prev = node
	list.head = node
}

// Append will add an element to the end of the list. This has linear
// comoplexity.
func (list *LinkedList) Append(data interface{}) {
	head := list.head

	if head.Null() {
		node := new(Node)
		node.Data = data
		node.Next = head
		node.Prev = nil
		head.Prev = node
		list.head = node
	} else {
		cursor := list.head
		for ; !cursor.Null(); cursor = cursor.Next {
		}
		node := new(Node)
		node.Prev = cursor.Prev
		node.Data = data
		node.Next = cursor
		cursor.Prev = node
		node.Prev.Next = node
	}
}

// Remove will remove the first instance of the target.
func (list *LinkedList) Remove(data interface{}) {
	cursor := list.head
	for ; !cursor.Null(); cursor = cursor.Next {
		if data == cursor.Data {
			if list.Len() == 1 {
				list.head = new(Node)
			} else if cursor.Prev == nil {
				list.head = cursor.Next
				list.head.Prev = nil
			} else if cursor.Next.Next == nil {
				prev := cursor.Prev
				prev.Next = cursor.Next
				cursor.Next.Prev = prev
			} else {
				prev := cursor.Prev
				next := cursor.Next
				next.Prev = prev
				prev.Next = next
			}
			break
		}
	}
}

// RemoveNode will remove a node.
func (list *LinkedList) RemoveNode(node *Node) {
	if node != nil {
		if list.Len() == 1 {
			list.head = new(Node)
		} else if node.Prev == nil {
			list.head = node.Next
			list.head.Prev = nil
		} else if node.Next.Next == nil {
			prev := node.Prev
			prev.Next = node.Next
			node.Next.Prev = prev
		} else {
			prev := node.Prev
			next := node.Next
			next.Prev = prev
			prev.Next = next
		}
	}
}

// InsertAfter will create a node with the given data and insert it after
// the node it is called on.
func (node *Node) InsertAfter(data interface{}) {
	target := new(Node)
	target.Data = data
	target.Next = node.Next
	target.Prev = node
	node.Next = target
}

// Tail returns the last element in the list.
func (list *LinkedList) Tail() *Node {
	cur := list.head
	tail := cur
	for ; !cur.Null(); cur = cur.Next {
		tail = cur
	}

	return tail
}
